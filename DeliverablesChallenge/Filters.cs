﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Element = Autodesk.Revit.DB.Element;
using GeometryElement = Autodesk.Revit.DB.GeometryElement;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Text.RegularExpressions;
using Quadrant = System.Int32;
using System.Data.SqlClient;

namespace DeliverablesChallenge
{
    public class UISelectionFilter
    {
        //Filter Selectable Elements by all BuiltInCategories in BuiltInCategories List
        public class SelectionFilter : ISelectionFilter
        {
            public List<BuiltInCategory> bics { get; set; }

            public bool AllowElement(Element element)
            {
                foreach(BuiltInCategory bic in bics)
                {
                    if (element.Category.Id.IntegerValue == (int)bic)
                    {
                        return true;
                    }
                }
                return false;
            }
            
            //Req'd to satsify Interface
            public bool AllowReference(Reference refer, XYZ point)
            {
                return false;
            }
        }
    }
}
