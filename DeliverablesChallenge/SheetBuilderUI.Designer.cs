﻿namespace DeliverablesChallenge
{
    partial class SheetBuilderUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxTitleblockSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDrawViewportRegion = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.rbCenter = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbCL = new System.Windows.Forms.RadioButton();
            this.rbCR = new System.Windows.Forms.RadioButton();
            this.rbBL = new System.Windows.Forms.RadioButton();
            this.rbBR = new System.Windows.Forms.RadioButton();
            this.rbBC = new System.Windows.Forms.RadioButton();
            this.rbTL = new System.Windows.Forms.RadioButton();
            this.rbTR = new System.Windows.Forms.RadioButton();
            this.rbTC = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelViewport = new System.Windows.Forms.Label();
            this.labelViewport2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNameFormat = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxTitleblockSelect
            // 
            this.comboBoxTitleblockSelect.FormattingEnabled = true;
            this.comboBoxTitleblockSelect.Location = new System.Drawing.Point(81, 15);
            this.comboBoxTitleblockSelect.Name = "comboBoxTitleblockSelect";
            this.comboBoxTitleblockSelect.Size = new System.Drawing.Size(346, 21);
            this.comboBoxTitleblockSelect.TabIndex = 0;
            this.comboBoxTitleblockSelect.SelectedIndexChanged += new System.EventHandler(this.comboBoxTitleblockSelect_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Titleblock";
            // 
            // buttonDrawViewportRegion
            // 
            this.buttonDrawViewportRegion.Location = new System.Drawing.Point(23, 98);
            this.buttonDrawViewportRegion.Name = "buttonDrawViewportRegion";
            this.buttonDrawViewportRegion.Size = new System.Drawing.Size(229, 27);
            this.buttonDrawViewportRegion.TabIndex = 2;
            this.buttonDrawViewportRegion.Text = "Draw Viewport Region";
            this.buttonDrawViewportRegion.UseVisualStyleBackColor = true;
            this.buttonDrawViewportRegion.Click += new System.EventHandler(this.buttonDrawViewportRegion_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(180, 310);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(99, 27);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // rbCenter
            // 
            this.rbCenter.AutoSize = true;
            this.rbCenter.Location = new System.Drawing.Point(62, 37);
            this.rbCenter.Name = "rbCenter";
            this.rbCenter.Size = new System.Drawing.Size(14, 13);
            this.rbCenter.TabIndex = 8;
            this.rbCenter.TabStop = true;
            this.rbCenter.UseVisualStyleBackColor = true;
            this.rbCenter.CheckedChanged += new System.EventHandler(this.rbCenter_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbCL);
            this.panel1.Controls.Add(this.rbCR);
            this.panel1.Controls.Add(this.rbBL);
            this.panel1.Controls.Add(this.rbBR);
            this.panel1.Controls.Add(this.rbBC);
            this.panel1.Controls.Add(this.rbTL);
            this.panel1.Controls.Add(this.rbTR);
            this.panel1.Controls.Add(this.rbTC);
            this.panel1.Controls.Add(this.rbCenter);
            this.panel1.Location = new System.Drawing.Point(6, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(135, 89);
            this.panel1.TabIndex = 9;
            // 
            // rbCL
            // 
            this.rbCL.AutoSize = true;
            this.rbCL.Location = new System.Drawing.Point(6, 37);
            this.rbCL.Name = "rbCL";
            this.rbCL.Size = new System.Drawing.Size(14, 13);
            this.rbCL.TabIndex = 16;
            this.rbCL.TabStop = true;
            this.rbCL.UseVisualStyleBackColor = true;
            this.rbCL.CheckedChanged += new System.EventHandler(this.rbCL_CheckedChanged);
            // 
            // rbCR
            // 
            this.rbCR.AutoSize = true;
            this.rbCR.Location = new System.Drawing.Point(115, 37);
            this.rbCR.Name = "rbCR";
            this.rbCR.Size = new System.Drawing.Size(14, 13);
            this.rbCR.TabIndex = 15;
            this.rbCR.TabStop = true;
            this.rbCR.UseVisualStyleBackColor = true;
            this.rbCR.CheckedChanged += new System.EventHandler(this.rbCR_CheckedChanged);
            // 
            // rbBL
            // 
            this.rbBL.AutoSize = true;
            this.rbBL.Location = new System.Drawing.Point(6, 69);
            this.rbBL.Name = "rbBL";
            this.rbBL.Size = new System.Drawing.Size(14, 13);
            this.rbBL.TabIndex = 14;
            this.rbBL.TabStop = true;
            this.rbBL.UseVisualStyleBackColor = true;
            this.rbBL.CheckedChanged += new System.EventHandler(this.rbBL_CheckedChanged);
            // 
            // rbBR
            // 
            this.rbBR.AutoSize = true;
            this.rbBR.Location = new System.Drawing.Point(115, 69);
            this.rbBR.Name = "rbBR";
            this.rbBR.Size = new System.Drawing.Size(14, 13);
            this.rbBR.TabIndex = 13;
            this.rbBR.TabStop = true;
            this.rbBR.UseVisualStyleBackColor = true;
            this.rbBR.CheckedChanged += new System.EventHandler(this.rbBR_CheckedChanged);
            // 
            // rbBC
            // 
            this.rbBC.AutoSize = true;
            this.rbBC.Location = new System.Drawing.Point(62, 69);
            this.rbBC.Name = "rbBC";
            this.rbBC.Size = new System.Drawing.Size(14, 13);
            this.rbBC.TabIndex = 12;
            this.rbBC.TabStop = true;
            this.rbBC.UseVisualStyleBackColor = true;
            this.rbBC.CheckedChanged += new System.EventHandler(this.rbBC_CheckedChanged);
            // 
            // rbTL
            // 
            this.rbTL.AutoSize = true;
            this.rbTL.Location = new System.Drawing.Point(6, 5);
            this.rbTL.Name = "rbTL";
            this.rbTL.Size = new System.Drawing.Size(14, 13);
            this.rbTL.TabIndex = 11;
            this.rbTL.TabStop = true;
            this.rbTL.UseVisualStyleBackColor = true;
            this.rbTL.CheckedChanged += new System.EventHandler(this.rbTL_CheckedChanged);
            // 
            // rbTR
            // 
            this.rbTR.AutoSize = true;
            this.rbTR.Location = new System.Drawing.Point(115, 5);
            this.rbTR.Name = "rbTR";
            this.rbTR.Size = new System.Drawing.Size(14, 13);
            this.rbTR.TabIndex = 10;
            this.rbTR.TabStop = true;
            this.rbTR.UseVisualStyleBackColor = true;
            this.rbTR.CheckedChanged += new System.EventHandler(this.rbTR_CheckedChanged);
            // 
            // rbTC
            // 
            this.rbTC.AutoSize = true;
            this.rbTC.Location = new System.Drawing.Point(62, 5);
            this.rbTC.Name = "rbTC";
            this.rbTC.Size = new System.Drawing.Size(14, 13);
            this.rbTC.TabIndex = 9;
            this.rbTC.TabStop = true;
            this.rbTC.UseVisualStyleBackColor = true;
            this.rbTC.CheckedChanged += new System.EventHandler(this.rbTC_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(279, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(148, 113);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "View Alignment In Region";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "No usable Titleblock Family found!";
            // 
            // labelViewport
            // 
            this.labelViewport.AutoSize = true;
            this.labelViewport.Location = new System.Drawing.Point(20, 182);
            this.labelViewport.Name = "labelViewport";
            this.labelViewport.Size = new System.Drawing.Size(35, 13);
            this.labelViewport.TabIndex = 12;
            this.labelViewport.Text = "label3";
            // 
            // labelViewport2
            // 
            this.labelViewport2.AutoSize = true;
            this.labelViewport2.Location = new System.Drawing.Point(20, 204);
            this.labelViewport2.Name = "labelViewport2";
            this.labelViewport2.Size = new System.Drawing.Size(35, 13);
            this.labelViewport2.TabIndex = 13;
            this.labelViewport2.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 254);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Sheet Name Format:";
            // 
            // textBoxNameFormat
            // 
            this.textBoxNameFormat.Location = new System.Drawing.Point(130, 251);
            this.textBoxNameFormat.Name = "textBoxNameFormat";
            this.textBoxNameFormat.Size = new System.Drawing.Size(286, 20);
            this.textBoxNameFormat.TabIndex = 15;
            this.textBoxNameFormat.TextChanged += new System.EventHandler(this.textBoxNameFormat_TextChanged);
            // 
            // SheetBuilderUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 349);
            this.Controls.Add(this.textBoxNameFormat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelViewport2);
            this.Controls.Add(this.labelViewport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonDrawViewportRegion);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxTitleblockSelect);
            this.Name = "SheetBuilderUI";
            this.Text = "SheetBuilderUI";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxTitleblockSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDrawViewportRegion;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.RadioButton rbCenter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbCL;
        private System.Windows.Forms.RadioButton rbCR;
        private System.Windows.Forms.RadioButton rbBL;
        private System.Windows.Forms.RadioButton rbBR;
        private System.Windows.Forms.RadioButton rbBC;
        private System.Windows.Forms.RadioButton rbTL;
        private System.Windows.Forms.RadioButton rbTR;
        private System.Windows.Forms.RadioButton rbTC;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelViewport;
        private System.Windows.Forms.Label labelViewport2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNameFormat;
    }
}