﻿using System;
using Autodesk.Revit.DB.ExtensibleStorage;


namespace DeliverablesChallenge
{
    public class ES
    {
        public static Guid storageId = new Guid("604b1052-ffff-ffff-ffff-fffffffff002");

        public static Schema ControlViewSchema()
        {
            SchemaBuilder sb = new SchemaBuilder(storageId);
            sb.SetSchemaName("ControlView");
            sb.SetVendorId("BirdSponge");
            sb.SetWriteAccessLevel(AccessLevel.Public);
            sb.SetReadAccessLevel(AccessLevel.Public);
            sb.SetDocumentation("ControlView Settings");

            FieldBuilder fbActiveSeries =
                sb.AddSimpleField("Series", typeof(string));
            fbActiveSeries.SetDocumentation("");

            return sb.Finish();
        }
    }
}
