﻿namespace DeliverablesChallenge
{
    partial class DeliverablesUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGenerateSheets = new System.Windows.Forms.Button();
            this.buttonSetupViewSet = new System.Windows.Forms.Button();
            this.buttonSetupSheets = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonGenerateSheets
            // 
            this.buttonGenerateSheets.Location = new System.Drawing.Point(27, 163);
            this.buttonGenerateSheets.Name = "buttonGenerateSheets";
            this.buttonGenerateSheets.Size = new System.Drawing.Size(236, 45);
            this.buttonGenerateSheets.TabIndex = 35;
            this.buttonGenerateSheets.Text = "Generate Set";
            this.buttonGenerateSheets.UseVisualStyleBackColor = true;
            this.buttonGenerateSheets.Click += new System.EventHandler(this.buttonGenerateSheets_Click);
            // 
            // buttonSetupViewSet
            // 
            this.buttonSetupViewSet.Location = new System.Drawing.Point(27, 22);
            this.buttonSetupViewSet.Name = "buttonSetupViewSet";
            this.buttonSetupViewSet.Size = new System.Drawing.Size(236, 42);
            this.buttonSetupViewSet.TabIndex = 36;
            this.buttonSetupViewSet.Text = "Setup View Set";
            this.buttonSetupViewSet.UseVisualStyleBackColor = true;
            this.buttonSetupViewSet.Click += new System.EventHandler(this.buttonSetupViewSet_Click);
            // 
            // buttonSetupSheets
            // 
            this.buttonSetupSheets.Location = new System.Drawing.Point(27, 81);
            this.buttonSetupSheets.Name = "buttonSetupSheets";
            this.buttonSetupSheets.Size = new System.Drawing.Size(236, 42);
            this.buttonSetupSheets.TabIndex = 37;
            this.buttonSetupSheets.Text = "Setup Sheets";
            this.buttonSetupSheets.UseVisualStyleBackColor = true;
            this.buttonSetupSheets.Click += new System.EventHandler(this.buttonSetupSheets_Click);
            // 
            // DeliverablesUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 226);
            this.Controls.Add(this.buttonSetupSheets);
            this.Controls.Add(this.buttonSetupViewSet);
            this.Controls.Add(this.buttonGenerateSheets);
            this.Name = "DeliverablesUI";
            this.Text = "Generate Sheet Series";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonGenerateSheets;
        private System.Windows.Forms.Button buttonSetupViewSet;
        private System.Windows.Forms.Button buttonSetupSheets;
    }
}