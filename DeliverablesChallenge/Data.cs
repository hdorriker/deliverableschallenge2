﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using Autodesk.Revit.UI.Selection;
using Element = Autodesk.Revit.DB.Element;
using System.Data;
using DeliverablesChallenge.Sheets;

namespace DeliverablesChallenge.Data
{
    public class Series
    {
        //This can be made into a list to handle multiple SheetSeries
        public static SheetSeries Active = null;

        //Process to Generate SheetSeries

    }
    public class ScopeBox
    {
        public Element RvtScopeBox { get; set; }
        public List<Curve> Boundaries { get; set; }

        public enum Id { Name, ESSFieldName, External }
        //Name = Just use the ScopeBox's UI-visible name
        //ESSFieldName = Look for a specific Field within the ScopeBox's ExtensibleStorage Schema (If schema doesn't exist, this should failover to NAME)
        //External = (Atypical)(For use where Extensible Storage is not permitted) Cross reference the Name of the ScopeBox against a List of aliases in a file outside the model.
        //              (Requires a path to be set for this file location)(This should failover to NAME)

        public enum Mode { Ignore, Append, Increment }
        //Ignore = Skip ScopeBoxes
        //Append = Tack the ScopeBoxId onto the end of the Sheet Number including separator (Separator should equal "" if no Separator) (Ex: A201.ZONE1, A201.ZONE2)
        //Increment = Apply the alternate increment to the Sheet Number for each scope (Ex: A201, A202, A203, A211, A212, A213)
        //            AlternateIncrement must be less than (Increment / Number of Areas)!

        public ScopeBox(Element RScopeBox)
        {
            RvtScopeBox = RScopeBox;
        }


        //Labeling
        public string GetIdent(Id id)
        {
            switch (id)
            {
                case Id.ESSFieldName:
                    //Open Transaction Required
                    Entity Ess = RvtScopeBox.GetEntity(Settings.DefineParameters.ScopeBoxes._Schema);
                    return Ess.Get<string>(Settings.DefineParameters.ScopeBoxes._LabelFieldName);
                case Id.External:
                    return Settings.DefineParameters.ScopeBoxes._ExternalAliases.GetAlias(RvtScopeBox);
                default:
                    return RvtScopeBox.Name;
            }
        }
    }
    public class Titleblock
    {
        public static List<FamilySymbol> Available = new List<FamilySymbol>();
        public static FamilySymbol Active = null;
        public static void Init(Document doc)
        {
            Available =
                new FilteredElementCollector(doc)
                    .OfClass(typeof(FamilySymbol))
                    .OfCategory(BuiltInCategory.OST_TitleBlocks)
                    .Cast<FamilySymbol>()
                    .ToList();
        }
        public static List<string> GetList()
        {
            List<string> nu = new List<string>();
            Available.ForEach(x => nu.Add(x.Name));
            return nu;
        }
        public static void SendSelected(int index)
        {
            Active = Available.ElementAt(index);
        }
    }
    public class ViewTemplate
    {
        public static List<View> Available = new List<View>();
        public static View Active = null;
        public static void Init(Document doc)
        {
            Available =
                new FilteredElementCollector(doc)
                    .OfClass(typeof(View))
                    .Cast<View>()
                    .Where(x => x.IsTemplate)
                    .Where(x => x.ViewType == ViewType.FloorPlan)
                    .ToList();
        }
        public static List<string> GetList()
        {
            List<string> nu = new List<string>();
            Available.ForEach(x => nu.Add(x.Name));
            return nu;
        }
        public static void SendSelected(int index)
        {
            Active = Available.ElementAt(index);
        }
    }
    public static class ViewPort
    {
        public enum Alignment { TopRight, TopCenter, TopLeft, CenterRight, TrueNeutral, CenterLeft, BottomRight, BottomCenter, BottomLeft };
        public static Alignment _Alignment;
        public static XYZ min = new XYZ();
        public static XYZ max = new XYZ();
        public static View ControlView = null;
        public static void DefineRegion()
        {
            ViewSheet temp = null;
            View last = Universal.core.uidoc.ActiveView;
            using (Transaction t = new Transaction(Universal.core.doc, " "))
            {
                t.Start();
                temp =
                    LocalToolkits.ViewCreation.NewViewSheet(
                        Universal.core.doc,
                        "Define Region",
                        "TEMP-DEFINEREGION",
                        Titleblock.Active
                        );
                Universal.core.doc.Regenerate();
                t.Commit();

                Universal.core.uidoc.ActiveView = temp;

                PickedBox pb = Universal.core.uidoc.Selection.PickBox(PickBoxStyle.Enclosing);
                min = pb.Min;
                max = pb.Max;

                Universal.core.uidoc.ActiveView = last;

                t.Start();
                Universal.core.doc.Delete(temp.Id);
                t.Commit();
            }
        }
        public static XYZ DeterminePlacement()
        {
            switch (_Alignment)
            {
                case Alignment.TrueNeutral:
                    return new XYZ(
                        ((0.5 * (max.X - min.X)) + min.X),
                        ((0.5 * (max.Y - min.Y)) + min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.CenterLeft:
                    return new XYZ(
                        (min.X),
                        ((0.5 * (max.Y - min.Y)) + min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.CenterRight:
                    return new XYZ(
                        (max.X),
                        ((0.5 * (max.Y - min.Y)) + min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.TopLeft:
                    return new XYZ(
                        (min.X),
                        (max.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.TopCenter:
                    return new XYZ(
                        ((0.5 * (max.X - min.X)) + min.X),
                        (max.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.TopRight:
                    return new XYZ(
                        (max.X),
                        (max.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.BottomLeft:
                    return new XYZ(
                        (min.X),
                        (min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.BottomCenter:
                    return new XYZ(
                        ((0.5 * (max.X - min.X)) + min.X),
                        (min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
                case Alignment.BottomRight:
                    return new XYZ(
                        (max.X),
                        (min.Y),
                        ((0.5 * (max.Z - min.Z)) + min.Z)
                        );
            }
            return new XYZ();
        }
        public static void CreateControlView()
        {
            using (Transaction t = new Transaction(Universal.core.doc, " "))
            {
                t.Start();
                //Create a ControlView which holds settings
                if (null != ControlView)
                {
                    //Remove old ControlView first
                    Universal.core.doc.Delete(ControlView.Id);
                }
                else
                {
                    //New ControlView
                    ControlView =
                        LocalToolkits.ViewCreation.NewViewPlan(
                            Universal.core.doc,
                            "ControlView",
                            LocalToolkits.LevelToolkit.OrderedLevelsInDoc.First(),
                            ViewType.DraftingView
                            );
                }
                Universal.core.doc.Regenerate();
                t.Commit();
            }
        }
        public static void SaveToControlView(string data)
        {
            using (Transaction t = new Transaction(Universal.core.doc, " "))
            {
                t.Start();
                Schema ess = ES.ControlViewSchema();
                Entity essData = new Entity(ess);
                Field fq = ess.GetField("Series");
                essData.Set<string>(fq, data);
                ControlView.SetEntity(essData);
                t.Commit();
            }
        }
    }
    public static class SeriesUI
    {
        //This is where incomplete data is parked for validation before making a SheetSeries class
        public static string SeriesName = "";
        public static string SeriesPrefix = "";
        public static string StartingNumber = "";
        public static int SeriesIncrement = 0;
        public static List<string> SelectedLevels = new List<string>();
        public static List<string> SelectedAreas = new List<string>();
        public static bool UseAreas = false;
        public static int Scale = 100;

        public static bool FromDeliverableUI = false;

    }
}
