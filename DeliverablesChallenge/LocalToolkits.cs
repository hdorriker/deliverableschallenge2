﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using Element = Autodesk.Revit.DB.Element;
using GeometryElement = Autodesk.Revit.DB.GeometryElement;
using System.Data;



namespace DeliverablesChallenge.LocalToolkits
{
    public static class SeparatorFormatting
    {
        public static List<string> Separators = new List<string>();
        public static string Found = "";
        public static List<string> IncludeSeparators()
        {
            if (Separators.Count < 1)
            {
                Separators.Add(".");
                Separators.Add("-");
                Separators.Add(":");
                Separators.Add("#");
                Separators.Add(",");
                Separators.Add("_");
                Separators.Add("~");
            }
            return Separators;
        }
        public static bool HasSeparator(string test)
        {
            foreach (string s in IncludeSeparators())
            {
                if (test.Contains(s))
                {
                    Found = s;
                    return true;
                }
            }
            return false;
        }
    }
    public static class CategorySets
    {
        public static List<BuiltInCategory> PlumbingSystemsInclusive()
        {
            List<BuiltInCategory> bic = new List<BuiltInCategory>();
            bic.Add(BuiltInCategory.OST_PipeFitting);
            bic.Add(BuiltInCategory.OST_PipeSegments);
            return bic;
        }
    }
    public static class SelectionToolkit
    {
        public static List<Element> UnpackSelected(Core core, List<BuiltInCategory> pass)
        {
            List<Element> FilterOut = new List<Element>();
            ICollection<ElementId> selIds = core.uidoc.Selection.GetElementIds();
            FilteredElementCollector coll = new FilteredElementCollector(core.doc, selIds);
            pass.ForEach(x => coll.OfCategory(x).ToList().ForEach(u => FilterOut.Add(u)));
            return FilterOut;
        }
        public static List<Element> FilteredUISelect(Core core, List<BuiltInCategory> pass)
        {
            List<Element> k = new List<Element>();
            try
            {
                UISelectionFilter.SelectionFilter qFilter = new UISelectionFilter.SelectionFilter();
                qFilter.bics = pass;
                k.Add(core.doc.GetElement(core.uidoc.Selection.PickObject(ObjectType.Element, qFilter).ElementId));
            }
            catch
            {
                //Selection ignored or has ended
            }
            return k;
        }
    }
    public static class ScopeToolkit
    {
        public static List<Element> GetScopeElements(Document doc)
        {
            List<Element> ScopeColl = new List<Element>();
            FilteredElementCollector rcollector = new FilteredElementCollector(doc);
            ICollection<Element> rcollection = rcollector.OfCategory(BuiltInCategory.OST_VolumeOfInterest).ToElements();
            foreach (var e in rcollection)
            {
                Element col = e as Element;

                if (null != col)
                {
                    ScopeColl.Add(col);
                }
            }
            return ScopeColl;
        }
        public static List<Curve> GetCurves(Element scope)
        {
            List<Curve> b = new List<Curve>();
            Options f = new Options();
            GeometryElement gg = scope.get_Geometry(f);

            foreach (GeometryObject geoObject in gg)
            {
                if (null != geoObject)
                {
                    b.Add(geoObject as Curve);
                }
            }

            return b;
        }
        public static BoundingBoxXYZ GetBoundingBox(Element scope, View cut)
        {
            return scope.get_BoundingBox(cut);
        }
        public static void ApplyScopeToView(View view, Element scope)
        {
            view.CropBox = GetBoundingBox(scope, view);
        }
    }
    public static class ProtectionToolkit
    {
        public static bool HasContent(List<Element> list)
        {
            try
            {
                if (list.Count > 0)
                {
                    return true;
                }
            }
            catch { }
            return false;
        }
    }
    public static class GeometryToolkit
    {
        public static XYZ GetLocationMultiMethod(Element ee)
        {
            XYZ g = new XYZ();
            try
            {
                //perform simple geometry lookup
                g = GetLocation(ee);
            }
            catch
            {
                //perform harder geometry extraction:
                g = GetCenterByGeometry(ee);
            }
            return g;
        }
        public static XYZ GetLocation(Element q)
        {
            Location[] locpt = new Location[1];
            int i = 0;
            for (i = 0; i < 1; i++)
            {
                locpt[i] = q.Location;
            }
            XYZ newLoc = new XYZ();
            foreach (LocationPoint loc in locpt)
            {
                newLoc = loc.Point;
            }
            return newLoc;
        }
        public static XYZ GetCenterByGeometry(Element c)
        {
            BoundingBoxXYZ b = GetRealBBFromElement(c);
            XYZ maxpt = b.Max;
            XYZ minpt = b.Min;

            //All the points
            double X_max = maxpt.X;
            double X_min = minpt.X;
            double Y_max = maxpt.Y;
            double Y_min = minpt.Y;
            double z = minpt.Z;

            //get midpoints
            double Xd = X_max - X_min;
            double Yd = Y_max - Y_min;

            double Xh = X_min + (Xd * 0.5d);
            double Yh = Y_min + (Yd * 0.5d);

            //center point
            return new XYZ(Xh, Yh, z);
        }
        public static BoundingBoxXYZ GetRealBBFromElement(Element c)
        {
            Options f = new Options();
            GeometryElement gg = c.get_Geometry(f);

            BoundingBoxXYZ k = gg.GetBoundingBox();
            return k;
        }
    }
    public static class LevelToolkit
    {
        public static List<Level> AllLevelsInDoc = new List<Level>();
        public static List<Level> OrderedLevelsInDoc = new List<Level>();
        public static void Init(Document doc)
        {
            //Gather all the levels in the document given
            AllLevelsInDoc = GetAllLevels(doc);

            //Order all levels from lowest to highest elevation
            OrderedLevelsInDoc = new List<Level>(); //make sure this list is FRESH
            OrderedLevelsInDoc = FindAndSortLevels(doc).ToList();
        }
        public static Level GetLevelFromView(Document doc, View active)
        {
            ElementId levelId = null;
            Parameter plevel = active.LookupParameter("Associated Level");

            foreach (Level lvl in AllLevelsInDoc)
            {
                if (lvl.Name == plevel.AsString())
                {
                    levelId = lvl.Id;
                }
            }

            Level k = null;

            if (levelId != null)
            {
                k = doc.GetElement(levelId) as Level;
            }

            return k;
        }
        public static IOrderedEnumerable<Level> FindAndSortLevels(Document doc)
        {
            return new FilteredElementCollector(doc)
                            .WherePasses(new ElementClassFilter(typeof(Level), false))
                            .Cast<Level>()
                            .OrderBy(e => e.Elevation);
        }
        public static List<Level> GetAllLevels(Document doc)
        {
            List<Level> k = new List<Level>();
            //Gather all the levels in the document given
            FilteredElementCollector lvlCollector = new FilteredElementCollector(doc);
            ICollection<Element> lvlCollection = lvlCollector.OfClass(typeof(Level)).ToElements();
            foreach (Level l in lvlCollection)
            {
                k.Add(l);
            }
            return k;
        }
        public static Level FindLevelOfElement(Document doc, Element e)
        {
            //GETS LEVEL BY ELEVATION OF ELEMENT, INSTEAD OF FROM ELEMENT'S REPORTED LEVEL

            //Get element location
            XYZ loc = GeometryToolkit.GetLocationMultiMethod(e);
            double cleanLoc = loc.Z;
            double errorThreshold = 0.00001;

            //protect against catastrophic non-zero/zero
            if (loc.Z < 0.002)
            {
                cleanLoc = 0;
            }

            if (OrderedLevelsInDoc.Count < 2)
            {
                //return first level if only one level exists
                return OrderedLevelsInDoc.ElementAt(0);
            }
            else
            {
                int i = 0;
                int tot = OrderedLevelsInDoc.Count;
                foreach (Level l in OrderedLevelsInDoc)
                {
                    if (i + 1 >= tot)
                    {
                        //End of list, no more levels exist to compare against, meaning this element belongs to top (last) level
                        return l;
                    }
                    else
                    {
                        //get level above
                        Level next = OrderedLevelsInDoc.ElementAt(i + 1);
                        Level now = l;

                        if (cleanLoc >= (now.Elevation - errorThreshold) && loc.Z < (next.Elevation - errorThreshold))
                        {
                            //assign this to the now elevation
                            return l;
                        }
                    }
                    i++;
                }
            }
            return OrderedLevelsInDoc.ElementAt(0); //if all else fails, return first level 
        }
        public static int LevelDifference(Document doc, Element a, Element b)
        {
            //find the how many Levels are between Element a and Element b
            Level La = FindLevelOfElement(doc, a);
            Level Lb = FindLevelOfElement(doc, b);

            int count = 0;
            bool counting = false;
            foreach (Level l in OrderedLevelsInDoc)
            {
                if (counting)
                {
                    count++;
                }

                if (l.Id.ToString().Equals(La.Id.ToString()))
                {
                    counting = true;
                }

                if (l.Id.ToString().Equals(Lb.Id.ToString()))
                {
                    counting = false;
                    return count;
                }
            }
            return count;
        }
        public static Level GetLevelFromName(Document doc, string name)
        {
            foreach (Level l in OrderedLevelsInDoc)
            {
                if (l.Name.Equals(name))
                {
                    return l;
                }
            }
            return null;
        }
        public static Level GetLevelFromElevation(Document doc, double elev)
        {
            foreach (Level L in LevelToolkit.OrderedLevelsInDoc)
            {
                if (Math.Abs(L.Elevation - elev) < 0.001)
                {
                    return L;
                }
            }
            throw new Exception("No Level Found At Elevation: " + elev);
        }
        public static bool isLevelBetweenLevels(Level test, Level min, Level max, bool inclusive)
        {
            if (
                (inclusive) &&
                (min.Elevation <= test.Elevation) &&
                (max.Elevation >= test.Elevation)
              )
            {
                return true;
            }
            if (
                (!inclusive) &&
                (min.Elevation < test.Elevation) &&
                (max.Elevation > test.Elevation)
              )
            {
                return true;
            }
            return false;
        }
        public static Level GetLevelAboveThisOne(Level current)
        {
            bool next = false;
            foreach (Level L in OrderedLevelsInDoc)
            {
                if (next)
                {
                    return L;
                }
                if (L.UniqueId.Equals(current.UniqueId))
                {
                    next = true;
                }
            }

            //no level above
            //just return the same level
            return current;
        }
    }
    public static class ViewCreation
    {
        public static IEnumerable<ViewFamilyType> FindViewTypes(Document doc, ViewType viewType)
        {
            IEnumerable<ViewFamilyType> vft = new FilteredElementCollector(doc)
                            .WherePasses(new ElementClassFilter(typeof(ViewFamilyType), false))
                            .Cast<ViewFamilyType>();

            switch (viewType)
            {
                case ViewType.AreaPlan:
                    return vft.Where(e => e.ViewFamily == ViewFamily.AreaPlan);
                case ViewType.CeilingPlan:
                    return vft.Where(e => e.ViewFamily == ViewFamily.CeilingPlan);
                case ViewType.ColumnSchedule:
                    return vft.Where(e => e.ViewFamily == ViewFamily.GraphicalColumnSchedule);
                case ViewType.CostReport:
                    return vft.Where(e => e.ViewFamily == ViewFamily.CostReport);
                case ViewType.Detail:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Detail);
                case ViewType.DraftingView:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Drafting);
                case ViewType.DrawingSheet:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Sheet);
                case ViewType.Elevation:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Elevation);
                case ViewType.EngineeringPlan:
                    return vft.Where(e => e.ViewFamily == ViewFamily.StructuralPlan);
                case ViewType.FloorPlan:
                    return vft.Where(e => e.ViewFamily == ViewFamily.FloorPlan);
                case ViewType.Legend:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Legend);
                case ViewType.LoadsReport:
                    return vft.Where(e => e.ViewFamily == ViewFamily.LoadsReport);
                case ViewType.PanelSchedule:
                    return vft.Where(e => e.ViewFamily == ViewFamily.PanelSchedule);
                case ViewType.PresureLossReport:
                    return vft.Where(e => e.ViewFamily == ViewFamily.PressureLossReport);
                case ViewType.Rendering:
                    return vft.Where(e => e.ViewFamily == ViewFamily.ImageView);
                case ViewType.Schedule:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Schedule);
                case ViewType.Section:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Section);
                case ViewType.ThreeD:
                    return vft.Where(e => e.ViewFamily == ViewFamily.ThreeDimensional);
                case ViewType.Walkthrough:
                    return vft.Where(e => e.ViewFamily == ViewFamily.Walkthrough);
                default:
                    return vft;
            }
        }

        public static View3D NewView3D(Document doc, string viewName)
        {
            ElementId viewTypeId = ViewCreation.FindViewTypes(doc, ViewType.ThreeD).First().Id;
            View3D v = View3D.CreateIsometric(doc, viewTypeId);
            v.Name = viewName;
            return v;
        }
        public static ViewPlan NewViewPlan(Document doc, string viewName, Level level, ViewType viewType)
        {
            ElementId viewTypeId = FindViewTypes(doc, viewType).First().Id;
            ViewPlan view = ViewPlan.Create(level.Document, viewTypeId, level.Id);
            view.Name = viewName;
            return view;
        }
        public static ViewSection NewViewSection(Document doc, string viewName, BoundingBoxXYZ sectBox, ViewType viewType)
        {
            ElementId viewTypeId = FindViewTypes(doc, viewType).First().Id;
            ViewSection view = ViewSection.CreateSection(doc, viewTypeId, sectBox);
            view.Name = viewName;
            return view;
        }
        public static ViewSheet NewViewSheet(Document doc, string sheetName, string sheetNumber, FamilySymbol Titleblock)
        {
            ElementId viewTypeId = FindViewTypes(doc, ViewType.DrawingSheet).First().Id;
            ViewSheet view = ViewSheet.Create(doc, Titleblock.Id);

            view.Name = sheetName;
            view.SheetNumber = sheetNumber;
            return view;
        }
    }
    public static class TitleblockToolkit
    {
        public static List<FamilySymbol> GetTitleblocks(Document doc)
        {
            FilteredElementCollector fec = new FilteredElementCollector(doc);
            fec.OfClass(typeof(FamilySymbol));
            fec.OfCategory(BuiltInCategory.OST_TitleBlocks);
            List<FamilySymbol> coll = new List<FamilySymbol>();
            foreach (var titleblock in fec)
            {
                if (null != titleblock)
                {
                    coll.Add(titleblock as FamilySymbol);
                }
            }
            return coll;
        }
    }
}
