﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliverablesChallenge
{
    public partial class ViewSeriesBuilderUI : Form
    {
        public bool IsValid = true;

        public ViewSeriesBuilderUI()
        {
            InitializeComponent();
            PopulateAreas();
            PopulateLevels();
            PopulateViewTemplates();
            labelLevelWarning.Text = "";
            maskedTextBoxScale.Text = "200";
            textBoxNameFormat.Text = "LEVELNAME AREAIDENTITY SERIESNAME FIXEDSTRING(PLAN)";
            textBoxNameFormat.Enabled = false;
            radioButtonAREA_Overall.Checked = true;
        }

        private void PopulateViewTemplates ()
        {
            comboBoxViewTemplate.Items.Clear();
            Data.ViewTemplate.GetList().ForEach(x => comboBoxViewTemplate.Items.Add(x + ""));
        }
        private void PopulateLevels()
        {
            checkedListBoxLevels.Items.Clear();
            LocalToolkits.LevelToolkit.OrderedLevelsInDoc.ForEach(x => checkedListBoxLevels.Items.Add(x.Name, false));
        }
        private void PopulateAreas()
        {
            checkedListBoxAreas.Items.Clear();
            LocalToolkits.ScopeToolkit.GetScopeElements(Universal.core.doc).ForEach(x => checkedListBoxAreas.Items.Add(x.Name, false));
        }
        private void ApplyAreaMode()
        {
            if(radioButtonAREA_Overall.Checked)
            {
                Data.SeriesUI.UseAreas = false;
                checkedListBoxAreas.Enabled = false;
            }
            else
            {
                Data.SeriesUI.UseAreas = true;
                checkedListBoxAreas.Enabled = true;
            }
        }
        private void ScanLevels()
        {
            Data.SeriesUI.SelectedLevels = new List<string>();
            foreach (var item in checkedListBoxLevels.CheckedItems)
            {
                string name = item.ToString();
                Data.SeriesUI.SelectedLevels.Add(name);
            }
            if(Data.SeriesUI.SelectedLevels.Count < 1)
            {
                //Minimum input not reached
                checkedListBoxLevels.BackColor = Color.Yellow;
                labelLevelWarning.Text = "Must Select At Least 1 Level!";
            }
        }
        private void ScanAreas()
        {
            Data.SeriesUI.SelectedAreas = new List<string>();
            foreach (var item in checkedListBoxAreas.CheckedItems)
            {
                string name = item.ToString();
                Data.SeriesUI.SelectedAreas.Add(name);
            }
        }
        private void comboBoxViewTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            Data.ViewTemplate.SendSelected(comboBoxViewTemplate.SelectedIndex);
        }

        private void checkedListBoxLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelLevelWarning.Text = "";
            checkedListBoxLevels.BackColor = Color.White;
        }

        private void tbSeries_TextChanged(object sender, EventArgs e)
        {

        }



        private void buttonGenerateViews_Click(object sender, EventArgs e)
        {
            ScanLevels();
            ScanAreas();

            IsValid = true;
            ValidateSeries();
            ValidateSeriesName();
            ValidateStartingNumber();

            if(IsValid)
            {
                //Commence View Creation and close
                Data.SeriesUI.SeriesName = textBoxSeriesName.Text.Trim();
                Data.SeriesUI.SeriesPrefix = textBoxSeries.Text.Trim();
                Data.SeriesUI.StartingNumber = maskedTextBoxStartNumber.Text.Trim();
                Data.SeriesUI.SeriesIncrement = Int32.Parse(maskedTextBoxIncrement.Text.Trim().TrimEnd(' '));
                Data.SeriesUI.Scale = Int32.Parse(maskedTextBoxScale.Text.Trim().TrimEnd(' '));
                Sheets.SheetNameFormat.LabelExpression = textBoxNameFormat.Text.Trim();

                this.Close();
            }
        }

        private void textBoxSeriesName_TextChanged(object sender, EventArgs e)
        {
            textBoxSeriesName.BackColor = Color.White;
        }
        private void ValidateSeriesName()
        {
            if(textBoxSeriesName.Text.Equals(""))
            {
                textBoxSeriesName.BackColor = Color.Yellow;
                IsValid = false;
            }
        }

        private void textBoxSeries_TextChanged(object sender, EventArgs e)
        {
            textBoxSeries.BackColor = Color.White;
        }
        private void ValidateSeries()
        {
            if(textBoxSeries.Text.Equals(""))
            {
                textBoxSeries.BackColor = Color.Yellow;
                IsValid = false;
            }
        }

        private void ValidateStartingNumber()
        {
            if(maskedTextBoxStartNumber.Text.Equals(""))
            {
                maskedTextBoxStartNumber.BackColor = Color.Yellow;
                IsValid = false;
            }
            else
            {
                maskedTextBoxStartNumber.BackColor = Color.White;
            }
            maskedTextBoxStartNumber.Text = maskedTextBoxStartNumber.Text.Trim();
        }

        private void maskedTextBoxStartNumber_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            //Don't do anything
        }

        private void ValidateIncrement()
        {
            if (maskedTextBoxIncrement.Text.Equals(""))
            {
                maskedTextBoxIncrement.BackColor = Color.Yellow;
                IsValid = false;
            }
            else
            {
                maskedTextBoxIncrement.BackColor = Color.White;
            }
            maskedTextBoxIncrement.Text = maskedTextBoxIncrement.Text.Trim();
        }

        private void maskedTextBoxIncrement_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            //Don't do anything
        }

        private void textBoxScale_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNameFormat_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButtonAREA_useScope_CheckedChanged(object sender, EventArgs e)
        {
            ApplyAreaMode();
        }
        private void radioButtonAREA_Overall_CheckedChanged(object sender, EventArgs e)
        {
            ApplyAreaMode();
        }

        private void maskedTextBoxScale_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
