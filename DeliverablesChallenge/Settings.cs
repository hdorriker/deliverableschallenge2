﻿using Autodesk.Revit.DB.ExtensibleStorage;

namespace DeliverablesChallenge
{
    namespace Settings
    {
        public class DefineParameters
        {
            public static string SeriesName = "";
            public static string SeriesSheetStart = "";
            public static int SeriesSheetIncrement = 1;


            public struct ScopeBoxes
            {
                public static ScopeBoxAlias _ExternalAliases = new ScopeBoxAlias();
                public static Schema _Schema = null;
                public static string _LabelFieldName = null;
                public static Data.ScopeBox.Id _Id = Data.ScopeBox.Id.Name;
                public static Data.ScopeBox.Mode _Mode = Data.ScopeBox.Mode.Append;
            }
        }
    }
}
