﻿namespace DeliverablesChallenge
{
    partial class ViewSeriesBuilderUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBoxLevels = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxAreas = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxSeriesName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonAREA_useScope = new System.Windows.Forms.RadioButton();
            this.radioButtonAREA_Overall = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelLevelWarning = new System.Windows.Forms.Label();
            this.comboBoxViewTemplate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonGenerateViews = new System.Windows.Forms.Button();
            this.maskedTextBoxStartNumber = new System.Windows.Forms.MaskedTextBox();
            this.textBoxSeries = new System.Windows.Forms.TextBox();
            this.maskedTextBoxIncrement = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.maskedTextBoxScale = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNameFormat = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListBoxLevels
            // 
            this.checkedListBoxLevels.FormattingEnabled = true;
            this.checkedListBoxLevels.Location = new System.Drawing.Point(6, 19);
            this.checkedListBoxLevels.Name = "checkedListBoxLevels";
            this.checkedListBoxLevels.Size = new System.Drawing.Size(241, 259);
            this.checkedListBoxLevels.TabIndex = 0;
            this.checkedListBoxLevels.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxLevels_SelectedIndexChanged);
            // 
            // checkedListBoxAreas
            // 
            this.checkedListBoxAreas.FormattingEnabled = true;
            this.checkedListBoxAreas.Location = new System.Drawing.Point(5, 65);
            this.checkedListBoxAreas.Name = "checkedListBoxAreas";
            this.checkedListBoxAreas.Size = new System.Drawing.Size(241, 214);
            this.checkedListBoxAreas.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(199, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Sheet Series:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(305, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Starting Number:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Series Name:";
            // 
            // textBoxSeriesName
            // 
            this.textBoxSeriesName.Location = new System.Drawing.Point(15, 34);
            this.textBoxSeriesName.Name = "textBoxSeriesName";
            this.textBoxSeriesName.Size = new System.Drawing.Size(177, 20);
            this.textBoxSeriesName.TabIndex = 13;
            this.textBoxSeriesName.TextChanged += new System.EventHandler(this.textBoxSeriesName_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonAREA_useScope);
            this.panel1.Controls.Add(this.radioButtonAREA_Overall);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 40);
            this.panel1.TabIndex = 14;
            // 
            // radioButtonAREA_useScope
            // 
            this.radioButtonAREA_useScope.AutoSize = true;
            this.radioButtonAREA_useScope.Location = new System.Drawing.Point(92, 9);
            this.radioButtonAREA_useScope.Name = "radioButtonAREA_useScope";
            this.radioButtonAREA_useScope.Size = new System.Drawing.Size(102, 17);
            this.radioButtonAREA_useScope.TabIndex = 1;
            this.radioButtonAREA_useScope.TabStop = true;
            this.radioButtonAREA_useScope.Text = "Scope To Areas";
            this.radioButtonAREA_useScope.UseVisualStyleBackColor = true;
            this.radioButtonAREA_useScope.CheckedChanged += new System.EventHandler(this.radioButtonAREA_useScope_CheckedChanged);
            // 
            // radioButtonAREA_Overall
            // 
            this.radioButtonAREA_Overall.AutoSize = true;
            this.radioButtonAREA_Overall.Location = new System.Drawing.Point(10, 9);
            this.radioButtonAREA_Overall.Name = "radioButtonAREA_Overall";
            this.radioButtonAREA_Overall.Size = new System.Drawing.Size(58, 17);
            this.radioButtonAREA_Overall.TabIndex = 0;
            this.radioButtonAREA_Overall.TabStop = true;
            this.radioButtonAREA_Overall.Text = "Overall";
            this.radioButtonAREA_Overall.UseVisualStyleBackColor = true;
            this.radioButtonAREA_Overall.CheckedChanged += new System.EventHandler(this.radioButtonAREA_Overall_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.checkedListBoxAreas);
            this.groupBox1.Location = new System.Drawing.Point(283, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 313);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Areas";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelLevelWarning);
            this.groupBox2.Controls.Add(this.checkedListBoxLevels);
            this.groupBox2.Location = new System.Drawing.Point(15, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 313);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Levels";
            // 
            // labelLevelWarning
            // 
            this.labelLevelWarning.AutoSize = true;
            this.labelLevelWarning.Location = new System.Drawing.Point(7, 288);
            this.labelLevelWarning.Name = "labelLevelWarning";
            this.labelLevelWarning.Size = new System.Drawing.Size(35, 13);
            this.labelLevelWarning.TabIndex = 1;
            this.labelLevelWarning.Text = "label2";
            // 
            // comboBoxViewTemplate
            // 
            this.comboBoxViewTemplate.FormattingEnabled = true;
            this.comboBoxViewTemplate.Location = new System.Drawing.Point(558, 34);
            this.comboBoxViewTemplate.Name = "comboBoxViewTemplate";
            this.comboBoxViewTemplate.Size = new System.Drawing.Size(212, 21);
            this.comboBoxViewTemplate.TabIndex = 17;
            this.comboBoxViewTemplate.SelectedIndexChanged += new System.EventHandler(this.comboBoxViewTemplate_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(555, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "View Template:";
            // 
            // buttonGenerateViews
            // 
            this.buttonGenerateViews.Location = new System.Drawing.Point(576, 338);
            this.buttonGenerateViews.Name = "buttonGenerateViews";
            this.buttonGenerateViews.Size = new System.Drawing.Size(177, 30);
            this.buttonGenerateViews.TabIndex = 19;
            this.buttonGenerateViews.Text = "Generate Views";
            this.buttonGenerateViews.UseVisualStyleBackColor = true;
            this.buttonGenerateViews.Click += new System.EventHandler(this.buttonGenerateViews_Click);
            // 
            // maskedTextBoxStartNumber
            // 
            this.maskedTextBoxStartNumber.Location = new System.Drawing.Point(308, 34);
            this.maskedTextBoxStartNumber.Mask = "099999";
            this.maskedTextBoxStartNumber.Name = "maskedTextBoxStartNumber";
            this.maskedTextBoxStartNumber.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBoxStartNumber.TabIndex = 20;
            this.maskedTextBoxStartNumber.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBoxStartNumber_MaskInputRejected);
            // 
            // textBoxSeries
            // 
            this.textBoxSeries.Location = new System.Drawing.Point(202, 34);
            this.textBoxSeries.Name = "textBoxSeries";
            this.textBoxSeries.Size = new System.Drawing.Size(100, 20);
            this.textBoxSeries.TabIndex = 21;
            this.textBoxSeries.TextChanged += new System.EventHandler(this.textBoxSeries_TextChanged);
            // 
            // maskedTextBoxIncrement
            // 
            this.maskedTextBoxIncrement.Location = new System.Drawing.Point(416, 34);
            this.maskedTextBoxIncrement.Mask = "099999";
            this.maskedTextBoxIncrement.Name = "maskedTextBoxIncrement";
            this.maskedTextBoxIncrement.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBoxIncrement.TabIndex = 23;
            this.maskedTextBoxIncrement.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBoxIncrement_MaskInputRejected);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(413, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Increment:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(558, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Scale:      1 : ";
            // 
            // maskedTextBoxScale
            // 
            this.maskedTextBoxScale.Location = new System.Drawing.Point(633, 65);
            this.maskedTextBoxScale.Mask = "099999";
            this.maskedTextBoxScale.Name = "maskedTextBoxScale";
            this.maskedTextBoxScale.Size = new System.Drawing.Size(51, 20);
            this.maskedTextBoxScale.TabIndex = 25;
            this.maskedTextBoxScale.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBoxScale_MaskInputRejected);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(558, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Name Format:";
            // 
            // textBoxNameFormat
            // 
            this.textBoxNameFormat.Location = new System.Drawing.Point(558, 114);
            this.textBoxNameFormat.Name = "textBoxNameFormat";
            this.textBoxNameFormat.Size = new System.Drawing.Size(212, 20);
            this.textBoxNameFormat.TabIndex = 27;
            this.textBoxNameFormat.TextChanged += new System.EventHandler(this.textBoxNameFormat_TextChanged);
            // 
            // ViewSeriesBuilderUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 383);
            this.Controls.Add(this.textBoxNameFormat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maskedTextBoxScale);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.maskedTextBoxIncrement);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSeries);
            this.Controls.Add(this.maskedTextBoxStartNumber);
            this.Controls.Add(this.buttonGenerateViews);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxViewTemplate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBoxSeriesName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Name = "ViewSeriesBuilderUI";
            this.Text = "ViewSeriesBuilderUI";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBoxLevels;
        private System.Windows.Forms.CheckedListBox checkedListBoxAreas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxSeriesName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonAREA_useScope;
        private System.Windows.Forms.RadioButton radioButtonAREA_Overall;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxViewTemplate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonGenerateViews;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxStartNumber;
        private System.Windows.Forms.TextBox textBoxSeries;
        private System.Windows.Forms.Label labelLevelWarning;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxIncrement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxScale;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNameFormat;
    }
}