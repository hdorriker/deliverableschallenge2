﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace DeliverablesChallenge
{
    public class Core
    {
        public Document doc { get; set; }
        public UIDocument uidoc { get; set; }
        public UIApplication uiapp { get; set; }
        public ExternalCommandData commandData { get; set; }
        public Application app { get; set; }

        public Core (ExternalCommandData commandData_)
        {
            commandData = commandData_;
            uiapp = commandData.Application;
            uidoc = commandData.Application.ActiveUIDocument;
            doc = commandData.Application.ActiveUIDocument.Document;
            app = commandData.Application.Application;
        }
    }
    public static class Universal
    {
        public static Core core = null;
    }
}
