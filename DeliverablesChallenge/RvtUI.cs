﻿using System;
using Autodesk.Revit.UI;
using System.Reflection;
using System.Windows.Media.Imaging;
namespace DeliverablesChallenge.SheetTools
{
    public class App : IExternalApplication
    {
        static string dllPath = "";
        static RibbonPanel ribPanel = null;

        static void AddRibbonPanel(UIControlledApplication uiapp)
        {
            //Create new RIBBON TAB if it doesn't already exist
            string tabName = "SheetTools";
            try
            {
                uiapp.CreateRibbonTab(tabName);
            }
            catch
            {
                //This tab may already exist and doesn't need to be made.
                //If so, Continue using the same tabName.
            }

            //DLL Path
            dllPath = Assembly.GetExecutingAssembly().Location;

            //Add Panel to Ribbon Tab
            ribPanel = uiapp.CreateRibbonPanel(tabName, "Deliverables");

            //Create Button for DELIVERABLES
            PushButtonData pbdSheetSeries =
                new PushButtonData(
                    "cmdSheetSeries",
                    "Makes Sheet\r\nSeries",
                    dllPath,
                    "DeliverablesChallenge.ExtCommands.MakeSheets"
                    );
            PushButton pbSheetSeries = ribPanel.AddItem(pbdSheetSeries) as PushButton;
            pbSheetSeries.ToolTip = "Create Series of Sheets";
            /*
            BitmapImage pbSheetSeriesIcon = 
                new BitmapImage(
                    new Uri("pack://application:,,,/AssemblySheetTool;component/Resources/FROMSELECTED.png")
                    );
            pbSheetSeries.LargeImage = pbSheetSeriesIcon;
            */
        }

        public Result OnShutdown(UIControlledApplication uiapp)
        {
            return Result.Succeeded;
        }
        public Result OnStartup(UIControlledApplication uiapp)
        {
            AddRibbonPanel(uiapp);
            return Result.Succeeded;
        }
    }
}
