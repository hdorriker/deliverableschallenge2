﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliverablesChallenge
{
    public partial class DeliverablesUI : Form
    {
        public DeliverablesUI()
        {
            InitializeComponent();
            Data.SeriesUI.FromDeliverableUI = true;
        }

        private void buttonSetupViewSet_Click(object sender, EventArgs e)
        {
            ViewSeriesBuilderUI vbi = new ViewSeriesBuilderUI();
            vbi.Show();
        }

        private void buttonSetupSheets_Click(object sender, EventArgs e)
        {
            SheetBuilderUI sbi = new SheetBuilderUI();
            sbi.Show();
        }

        private void buttonGenerateSheets_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbByLevel_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbByArea_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tbStartNumber_TextChanged(object sender, EventArgs e)
        {
            
        }


        private void buttonDrawNewScope_Click(object sender, EventArgs e)
        {

        }


    }
}
