﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverablesChallenge.Sheets
{
    public class SheetNumberFormat
    {
        //Ruleset - This generally doesn't change from sheet to sheet------------------------------------------------------------------//
        public static string Separator { get; set; } //Set by UI
        public static int AltIncrement { get; set; } //Set by UI
        public static Data.ScopeBox.Id ScopeBoxIdentity { get; set; } //Set by Config and/or UI
        public static Data.ScopeBox.Mode ScopeBoxExpressionMode { get; set; } //Set by Config and/or UI
        //-----------------------------------------------------------------------------------------------------------------------------//

        internal static string SeriesPrefix = ""; //If the series starting number has a separator in it (Ex: A2.01 Prefix = "A2.")
        internal static string PrimarySheet { get; set; } //Set by methods

        public static void EnumerateSheet(
            SheetSeries Series,
            ViewSheet Sheet,
            int LevelNumber,
            int AreaNumber = -1
            )
        {
            //Open Transaction Required

            int chars = Series.SheetStart.Length;
            int indexInSeries = (LevelNumber * Series.IncludedAreas.Count) + AreaNumber;

            //Make Base Sheet Number (Level)
            int sheetNumber =
                Int32.Parse(Series.SheetStart.Trim().TrimStart(new Char[] { '0' })) + (LevelNumber * Series.SheetIncrement);

            string nu = Series.SeriesPrefix;
            if (Series.UseAreas)
            {
                //Modify For Area
                switch (ScopeBoxExpressionMode)
                {
                    case Data.ScopeBox.Mode.Increment:
                        sheetNumber += (AltIncrement * (AreaNumber + 1));
                        nu += ReformatForPreZeros(sheetNumber, chars);
                        break;
                    case Data.ScopeBox.Mode.Append:
                        nu +=
                            sheetNumber.ToString() + Separator +
                            Series.IncludedAreas.ElementAt(AreaNumber).GetIdent(ScopeBoxIdentity);
                        break;
                }
            }
            else
            {
                nu += sheetNumber.ToString();
            }
            
            Sheet.SheetNumber = nu;
        }
        private static string ReformatForPreZeros(int sheetNumber, int length)
        {
            string nu = sheetNumber.ToString();
            while (nu.Length < length)
            {
                nu = "0" + nu;
            }
            return nu;
        }
    }

}
