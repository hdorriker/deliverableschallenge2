﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverablesChallenge.Sheets
{
    public static class Deliverable
    {
        public static void Build (SheetSeries series, bool MakeSheets)
        {
            for (int levelItr = 0; levelItr < Data.SeriesUI.SelectedLevels.Count;)
            {
                int areaItr = -1;
                if (series.UseAreas)
                {
                    areaItr = 0;
                }
                while (areaItr < series.IncludedAreas.Count)
                {
                    View nuView = null;
                    using (Transaction t = new Transaction(Universal.core.doc, "NewView"))
                    {
                        t.Start();
                        nuView = series.CreatePlanView(
                            Universal.core.doc,
                            "Temp",
                            series.IncludedLevels
                                .ElementAt(levelItr)
                                );
                        Universal.core.doc.Regenerate();
                        t.Commit();
                    }
                    using (Transaction t = new Transaction(Universal.core.doc, "NewView"))
                    {
                        t.Start();
                        nuView.ViewTemplateId = series._Template.Id;
                        Universal.core.doc.Regenerate();
                        t.Commit();
                    }
                    if (series.UseAreas)
                    {
                        using (Transaction t = new Transaction(Universal.core.doc,"ApplyArea"))
                        {
                            t.Start();
                            nuView.CropBoxActive = true;
                            nuView.CropBox =
                                series.IncludedAreas
                                    .ElementAt(areaItr)
                                    .RvtScopeBox
                                        .get_BoundingBox(nuView);
                            Universal.core.doc.Regenerate();
                            t.Commit();
                        }   
                    }
                    using (Transaction t = new Transaction(Universal.core.doc, "NewView"))
                    {
                        t.Start();
                        SheetNameFormat.NameSheet(
                            series,
                            nuView,
                            levelItr,
                            areaItr
                            );
                        Universal.core.doc.Regenerate();
                        t.Commit();
                    }

                    if(MakeSheets)
                    {
                        ViewSheet nuSht = null;
                        using (Transaction t = new Transaction(Universal.core.doc, "NewSheet"))
                        {
                            t.Start();
                             nuSht = series.CreateViewSheet(
                                Universal.core.doc,
                                "TempSheet",
                                "Temp0"
                                );
                            Universal.core.doc.Regenerate();
                            t.Commit();
                        }
                        using (Transaction t = new Transaction(Universal.core.doc, "NewSheet"))
                        {
                            t.Start();
                            SheetNumberFormat.EnumerateSheet(
                                series,
                                nuSht,
                                levelItr,
                                areaItr
                                );
                            SheetNameFormat.NameSheet(
                                series,
                                nuSht,
                                levelItr,
                                areaItr
                                );
                            Universal.core.doc.Regenerate();
                            t.Commit();
                        }
                        using (Transaction t = new Transaction(Universal.core.doc, "NewSheet"))
                        {
                            t.Start();
                            Viewport.Create(Universal.core.doc, nuSht.Id, nuView.Id, series.Placement);
                            Universal.core.doc.Regenerate();
                            t.Commit();
                        }
                    }
                    areaItr++;
                }
                levelItr++;
            }
        }
    }
}
