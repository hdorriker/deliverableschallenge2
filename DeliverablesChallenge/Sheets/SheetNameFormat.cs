﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverablesChallenge.Sheets
{
    public class SheetNameFormat
    {
        public static string LabelExpression { get; set; }
        //Ex: LEVEL 02 AREA 3 PLUMBING PLAN
        //LabelExpression = LEVELNAME AREAIDENTITY SERIESNAME FIXEDSTRING(PLAN)


        public static void NameSheet(
            SheetSeries Series,
            View Sheet,
            int LevelNumber,
            int AreaNumber = -1
            )
        {
            //Open Transaction Required
            string name = "";

            //Parse LabelExpression
            List<string> comps = new List<string>();
            comps = LabelExpression.Split(' ').ToList();
            foreach (string item in comps)
            {
                string cleaned = item.Trim();

                if (cleaned.Contains("FIXEDSTRING"))
                {
                    string content = cleaned.Split('(').ToList().ElementAt(1);
                    name += content.TrimEnd(')').Trim() + " ";
                }
                else
                {
                    switch (cleaned)
                    {
                        case "LEVELNAME":
                            //Get Name of the level
                            name += Series.IncludedLevels.ElementAt(LevelNumber).Name + " ";
                            break;
                        case "SERIESNAME":
                            //Name of the Series
                            name += Series.Name + " ";
                            break;
                        case "AREAIDENTITY":
                            //Identity of Area
                            if(AreaNumber > -1)
                            {
                                name += Series.IncludedAreas.ElementAt(AreaNumber).RvtScopeBox.Name + " ";
                            }
                            else
                            {
                                //don't use Area
                            }
                            break;
                    }
                }
            }
            Sheet.Name = name.Trim();
        }
    }
}
