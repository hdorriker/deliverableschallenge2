﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Element = Autodesk.Revit.DB.Element;
using GeometryElement = Autodesk.Revit.DB.GeometryElement;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;


namespace DeliverablesChallenge.Sheets
{
    public class SheetSeries
    {
        public List<ViewSheet> Sheets = new List<ViewSheet>();

        public string Name = ""; //Name of the Series, Used for View and Sheet Naming
        public string SeriesPrefix = "";
        public string SheetStart = "0";
        public int SheetIncrement = 1;
        public ViewType ViewTypeEnum = ViewType.FloorPlan;
        public bool UseAreas = false;

        public View _Template { get; set; }
        public ViewDetailLevel _DetailLevel = ViewDetailLevel.Medium;
        public ViewDiscipline _Discipline = ViewDiscipline.Coordination;
        public ViewTemplateApplicationOption _TemplateOptions = ViewTemplateApplicationOption.AllParameters;
        public int _Scale { get; set; }
        public XYZ Placement { get; set; }

        public List<Level> IncludedLevels { get; set; }
        public List<Data.ScopeBox> IncludedAreas { get; set; }

        public FamilySymbol Titleblock { get; set; }

        public SheetSeries(
            string _SeriesName,
            string _SheetStart,
            int _SheetIncrement,
            string _SeriesPrefix = ""
            )
        {
            //Name = Name of the sheet series (Example: "Architectural")
            //SheetStart = SheetNumber of first Sheet in the series (Example: "A200")
            //SheetIncrement = Increment for each subsequent sheet (Example: "A200, A210: increment = 10)
            //SeriesPrefix = The Characters prior to the part of the sheet number that increments.(Ex: A2.01 Prefix = "A2.")

            Name = _SeriesName;
            SheetStart = _SheetStart;
            SheetIncrement = _SheetIncrement;
            SeriesPrefix = _SeriesPrefix;
            IncludedLevels = new List<Level>();
            IncludedAreas = new List<Data.ScopeBox>();
        }
        public void LoadSelectedAreas(List<string> Areas)
        {
            if(Areas.Count < 1)
            {
                //skip this and ignore areas
                UseAreas = false;
            }
            else
            {
                Areas.ForEach(x => IncludedAreas.Add(
                new Data.ScopeBox(
                    LocalToolkits.ScopeToolkit.GetScopeElements(Universal.core.doc)
                        .Where(y => y.Name.Equals(x))
                        .First()
                        )
                    )
                    );
                UseAreas = true;
            }
        }
        public void LoadSelectedLevels(List<string> RLevels)
        {
            RLevels.ForEach(x => IncludedLevels.Add(
                    LocalToolkits.LevelToolkit.OrderedLevelsInDoc
                        .Where(y => y.Name.Equals(x))
                        .First()
                        )
                    );
        }



        //Deliverable Creation Methods
        public ViewSheet CreateViewSheet(Document doc, string name, string number)
        {
            ViewSheet sh = LocalToolkits.ViewCreation.NewViewSheet(doc, name, number, Titleblock);
            return sh;
        }
        public View CreatePlanView(Document doc, string name, Level level)
        {
            //Open Transaction Required
            View nu = LocalToolkits.ViewCreation.NewViewPlan(doc, name, level, ViewTypeEnum);

            //Apply Params
            nu.Scale = _Scale;
            nu.DetailLevel = _DetailLevel;
            nu.Discipline = _Discipline;

            return nu;
        }
    }
}
