﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Element = Autodesk.Revit.DB.Element;
using GeometryElement = Autodesk.Revit.DB.GeometryElement;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Text.RegularExpressions;
using System.Reflection;

namespace DeliverablesChallenge.ExtCommands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]

    class MakeSheets : IExternalCommand
    {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData, ref string message, ElementSet elementSet)
        {
            //Basics
            Universal.core = new Core(commandData);
            LocalToolkits.LevelToolkit.Init(Universal.core.doc);
            Data.ViewTemplate.Init(Universal.core.doc);
            Data.Titleblock.Init(Universal.core.doc);

            //Launch the new system window
            System.Windows.Forms.Application.Run(new DeliverablesUI());

            //Make Deliverables
            Sheets.SheetSeries InstanceSeries =
            new Sheets.SheetSeries(
                Data.SeriesUI.SeriesName,
                Data.SeriesUI.StartingNumber,
                Data.SeriesUI.SeriesIncrement,
                Data.SeriesUI.SeriesPrefix
                );

            InstanceSeries.Placement = Data.ViewPort.DeterminePlacement();
            InstanceSeries.LoadSelectedAreas(Data.SeriesUI.SelectedAreas);
            InstanceSeries.LoadSelectedLevels(Data.SeriesUI.SelectedLevels);
            InstanceSeries.UseAreas = Data.SeriesUI.UseAreas;
            InstanceSeries._Scale = Data.SeriesUI.Scale;
            InstanceSeries._Template = Data.ViewTemplate.Active;
            InstanceSeries.Titleblock = Data.Titleblock.Active;

            //Hard Coded Setting
            Sheets.SheetNumberFormat.ScopeBoxExpressionMode = Data.ScopeBox.Mode.Append;
            Sheets.SheetNumberFormat.ScopeBoxIdentity = Data.ScopeBox.Id.Name;
            Sheets.SheetNumberFormat.Separator = "-";

            Sheets.Deliverable.Build(InstanceSeries, true);

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
