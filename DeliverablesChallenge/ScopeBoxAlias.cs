﻿using System;
using System.Collections.Generic;
using System.Linq;
using Element = Autodesk.Revit.DB.Element;
using System.IO;

namespace DeliverablesChallenge
{
    public class ScopeBoxAlias
    {
        public string ScopeBoxAliasPath = "";
        public List<Alias> AliasList = new List<Alias>();
        public void SetScopeBoxAliasPath(string path)
        {
            List<string> lines = File.ReadAllLines(path).ToList();
            if (!lines.First().Equals("Revit ScopeBox Name : ScopeBox Alias"))
            {
                //Unexpected Directory File, Failover to ScopeBoxName
            }
            else
            {
                AliasList = new List<Alias>(); //Start over with empty list
                lines.ForEach(x => AliasList.Add(new Alias(x)));
            }
        }
        public string GetAlias(Element RvtScopeBox)
        {
            foreach(Alias a in AliasList)
            {
                if(a.RevitName.Equals(RvtScopeBox.Name))
                {
                    return a.LabelName;
                }
            }
            //If it gets here, there is no alias. Failover to Revit Name
            return RvtScopeBox.Name;
        }
    }
    public struct Alias
    {
        public string RevitName { get; set; }
        public string LabelName { get; set; }
        public Alias(string line)
        {
            List<string> comps = line.Split(':').ToList();
            RevitName = comps.First();
            LabelName = comps.Last();
        }
    }
}
