﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliverablesChallenge
{
    public partial class SheetBuilderUI : Form
    {
        public SheetBuilderUI()
        {
            InitializeComponent();
            label2.Text = "";
            PopulateTitleblockSelections();
            rbCenter.Checked = true;
            buttonDrawViewportRegion.Enabled = false;
            labelViewport.Text = "";
            labelViewport2.Text = "";
            textBoxNameFormat.Text = "LEVELNAME AREAIDENTITY SERIESNAME FIXEDSTRING(PLAN)";
            textBoxNameFormat.Enabled = false;
            try
            {
                comboBoxTitleblockSelect.SelectedIndex = 0; //This must always have a selection
            }
            catch
            {
                label2.Text = "No Usable Titleblock Family Found!";
                comboBoxTitleblockSelect.Enabled = false;
                buttonSave.Enabled = false;
            }
        }

        private void PopulateTitleblockSelections()
        {
            comboBoxTitleblockSelect.Items.Clear();
            Data.Titleblock.GetList().ForEach(x => comboBoxTitleblockSelect.Items.Add(x + ""));
        }
        private void PollAlignmentSettings()
        {
            if(rbBC.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.BottomCenter;
            }
            if (rbBR.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.BottomRight;
            }
            if (rbBL.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.BottomLeft;
            }
            if (rbCenter.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.TrueNeutral;
            }
            if (rbCR.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.CenterRight;
            }
            if (rbCL.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.CenterLeft;
            }
            if (rbTC.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.TopCenter;
            }
            if (rbTR.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.TopRight;
            }
            if (rbTL.Checked)
            {
                Data.ViewPort._Alignment = Data.ViewPort.Alignment.TopLeft;
            }
        }

        private void comboBoxTitleblockSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonDrawViewportRegion.Enabled = true;
            Data.Titleblock.SendSelected(comboBoxTitleblockSelect.SelectedIndex);
            
        }

        private void rbPLAN_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Data.Titleblock.SendSelected(comboBoxTitleblockSelect.SelectedIndex);
            Sheets.SheetNameFormat.LabelExpression = textBoxNameFormat.Text.Trim();
            this.Close();
        }

        private void rbCenter_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbTL_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbCL_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbCR_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbBL_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbBC_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbBR_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbTC_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void rbTR_CheckedChanged(object sender, EventArgs e)
        {
            PollAlignmentSettings();
        }

        private void buttonDrawViewportRegion_Click(object sender, EventArgs e)
        {
            //Launch UI box selection
            Data.ViewPort.DefineRegion();
            labelViewport.Text = "Max = " + Data.ViewPort.max;
            labelViewport2.Text = "Min = " + Data.ViewPort.min;
        }

        private void textBoxNameFormat_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
